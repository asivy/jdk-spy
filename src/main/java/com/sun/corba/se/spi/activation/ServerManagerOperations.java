package com.sun.corba.se.spi.activation;

/**
* com/sun/corba/se/spi/activation/ServerManagerOperations.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from c:/re/workspace/8-2-build-windows-amd64-cygwin/jdk8u20/731.nc/corba/src/share/classes/com/sun/corba/se/spi/activation/activation.idl
* Wednesday, June 25, 2014 12:29:17 AM PDT
*/

public interface ServerManagerOperations extends com.sun.corba.se.spi.activation.ActivatorOperations, com.sun.corba.se.spi.activation.LocatorOperations {
} // interface ServerManagerOperations
