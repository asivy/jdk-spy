package org.omg.PortableServer;

/**
* org/omg/PortableServer/ImplicitActivationPolicy.java .
* Generated by the IDL-to-Java compiler (portable), version "3.2"
* from c:/re/workspace/8-2-build-windows-amd64-cygwin/jdk8u20/731.nc/corba/src/share/classes/org/omg/PortableServer/poa.idl
* Wednesday, June 25, 2014 12:29:17 AM PDT
*/

/**
	 * This policy specifies whether implicit activation 
	 * of servants is supported in the created POA.
	 */
public interface ImplicitActivationPolicy extends ImplicitActivationPolicyOperations, org.omg.CORBA.Policy, org.omg.CORBA.portable.IDLEntity {
} // interface ImplicitActivationPolicy
